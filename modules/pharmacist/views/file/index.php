<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\FileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Files');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'label' => 'Nombre del Evento',
                'attribute' => 'event_id',
                'value' => 'event.name',
            ],
            [
                'label' => 'Logo',
                'attribute' => 'event_id',
                'format' => 'html',
                'value' => function($dataProvider){
                    return Html::img($dataProvider->event->getImageSrc('small_'));
                }
            ],
            [
                'label' => 'Lugar del evento',
                'attribute' => 'event_id',
                'value' => 'event.place',
            ],
            'created_at:date',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        return Url::to('@web/' . $model->path);
                    }
                }
            ]
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
