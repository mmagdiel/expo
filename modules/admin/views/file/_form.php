<?php

use yii\helpers\Html;
use app\models\Event;
use app\models\Category;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($file, 'file')->fileInput() ?>

    <?= $form->field($event, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($event, 'logo')->widget('demi\image\FormImageWidget', [
        'imageSrc' => $event->getImageSrc('medium_'),
    ]) ?>

    <?= $form->field($event, 'date_from')->widget(DateRangePicker::className(), [
        'attributeTo' => 'date_to', 
        'form' => $form, // best for correct client validation
        'language' => 'es',
        'size' => 'md',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ] 
    ]) ?>

    <?= $form->field($event, 'place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($tags, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una region'])  
    ?>

    <div class="form-group">
        <?= Html::submitButton($file->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $file->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
