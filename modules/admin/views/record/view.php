<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Record */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Records'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'company',
            'contact_information:ntext',
            'address:ntext',
            'products',
            'company_type',
            'category',
            'place',
            'latin_america',
            'note:ntext',
            'email:email',
            'date_first_contact',
            'date_answer',
            'city',
            'price_list',
            'product_catalog',
            'application_of_sanitary',
            'apostilled_legal',
            'clv',
            'manuals',
            'iso',
            'ce',
            'fda',
            'material_analysis',
            'tariff_classification',
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute'=>'updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->updated_by)->username;
                },
            ],
            [
                'attribute' => 'event_id',
                'value' => function($model) {
                    return $model->event->name;
                }
            ],
        ],
    ]) ?>
</div>
