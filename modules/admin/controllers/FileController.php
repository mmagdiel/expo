<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\File;
use app\models\Event;
use app\models\Tags;
use app\models\search\FileSearch;
use app\models\search\ExcelSearch;
use app\models\search\PdfSearch;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;
use yii\web\UnsupportedMediaTypeHttpException;

/**
 * FileController implements the CRUD actions for File model.
 */
class FileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FileSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionReport()
    {
        $searchModel = new PdfSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all File models.
     * @return mixed
     */
    public function actionData()
    {
        $searchModel = new ExcelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
 
    /**
     * Displays a single File model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $file = new File();
        $event = new Event();
        $tags = new Tags();
        $this->handle($file, $event, $tags);
        
        return $this->render('create', [
            'file' => $file,
            'event' => $event, 
            'tags' => $tags
        ]);
    }

    /**
     * Creates a new File model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpload()
    {
        if (Yii::$app->user->isGuest) {
            throw new UnauthorizedHttpException("Error!!!!");
        }

        $file = new File();
        $this->handlee($file);
        
        return $this->render('upload', [
            'file' => $file,
        ]);
    }

    /**
     * Updates an existing File model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing File model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the File model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return File the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = File::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Upload file to the system
     */ 
    protected function handle(File $model, $event, $tags)
    {
        if ($model->load(Yii::$app->request->post()) && $event->load(Yii::$app->request->post()) && $tags->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model,'file');
            if (!isset($model->file)) {
                throw new NotFoundHttpException("Error Processing the File");
            }
            $ext = $model->file->extension;
            if(strcmp($ext,'xls') * strcmp($ext,'xlsx') != 0 ) {
                throw new UnsupportedMediaTypeHttpException("Error Processing Request");
            }
            $token = Yii::$app->security->generateRandomString(23);
            $model->path = 'excel/' . $token . '.' . $ext;
            $model->file->saveAs($model->path);
            $model->error = $model->file->error;
            if ($model->error != '0') {
                throw new NotFoundHttpException("Error Saved the File");
            }
            $model->size = $model->file->size;
            $model->name = $model->file->name;
            $model->type = 'Datos';
            if ($event->save(false) ) {
                $model->event_id = $event->id;
                if ($model->save(false) ) {
                    // Se necesita convertir de tipo a entero, por el uso de ArrayHelper::map se cambiaron a tipo string 
                    $tags->event_id = $model->event_id;
                    $tags->category_id = (int)$tags->category_id;
                    if ( $tags->save(false) ) {
                        $id = $model->id;
                        $idE = $event->id;
                        $user = Yii::$app->get('user', false);
                        $user = $user && !$user->isGuest ? $user->id : 0;
                        // Ojo: en la base de dato el Usurio 0 es por defecto, no debe de existir otro usuario real con ese id
                        $runner = new \tebazil\runner\ConsoleCommandRunner();
                        $runner->run('upload/record', [$id, $idE, $user]);
                        $output = $runner->getOutput();
                        $exitCode = $runner->getExitCode();
                        return $this->redirect(['view', 
                           'id' => $id ]);
                    }                  
                }
            }
        }
    }

    /**
     * Upload file to the system
     */ 
    protected function handlee(File $model)
    {
        if ($model->load(Yii::$app->request->post()) && $tags->load(Yii::$app->request->post()) ) {
            $model->file = UploadedFile::getInstance($model,'file');
            if (!isset($model->file)) {
                throw new NotFoundHttpException("Error Processing the File");
            }
            $ext = $model->file->extension;
            print("<pre>".print_r($ext,true)."</pre>");
            if(strcmp($ext,'pdf') != 0 ) {
                throw new UnsupportedMediaTypeHttpException("Error Processing Request");
            }
            $token = Yii::$app->security->generateRandomString(23);
            $model->path = 'pdf/' . $token . '.' . $ext;
            $model->file->saveAs($model->path);
            $model->error = $model->file->error;
            if ($model->error != '0') {
                throw new NotFoundHttpException("Error Saved the File");
            }
            $model->size = $model->file->size;
            $model->name = $model->file->name;
            $model->type = 'Documentos';

            $model->event_id = (int)$tags->event_id;
            if ($model->save(false) ) {
                $id = $model->id;
                return $this->redirect(['view', 
                   'id' => $id ]);
            }
        }
    }
}
