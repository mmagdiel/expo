<?php

namespace app\modules\cosmetics;

/**
 * cosmetics module definition class
 */
class Cosmetics extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\cosmetics\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
