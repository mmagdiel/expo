<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\Record;
use app\models\File;

/* @var $this yii\web\View */
/* @var $model app\models\Event */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'name',
            [
                'attribute' => 'Logo',
                'format' => 'html',
                'value' => function($dataProvider){
                    return Html::img($dataProvider->getImageSrc('small_'));
                }
            ],
            'date_from',
            'date_to',
            'place:ntext',
            [
                'attribute' => 'id',
                'label' => Yii::t('app','Number of Records'),
                'value' => function($model) {
                    return Record::find()->where(['event_id' => $model->id])->count();
                }
            ],
            [
                'attribute' => 'id',
                'label' => Yii::t('app','Number of Documents'),
                'value' => function($model) {
                    return File::find()->where(['event_id' => $model->id])->andWhere(['type' => 'Documents'])->count();
                }
            ],
            'created_at:datetime',
            [
                'attribute'=>'created_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->created_by)->username;
                },
            ],
            'updated_at:datetime',
            [
                'attribute'=>'updated_by',
                'value'=>function($dataProvider){
                    return User::findIdentity($dataProvider->updated_by)->username;
                },
            ],
        ],
    ]) ?>

</div>
