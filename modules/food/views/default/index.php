<?php 
use kartik\icons\Icon;
Icon::map($this);  
?>

<div class="care-default-index">
<!--    <h1><?= $this->context->action->uniqueId ?></h1> -->

    <div class="row">
        <div class="col-md-6">
            <div class="jumbotron">
                <h3> Muestra </h3>
                <?= Icon::show('eye', ['class' => 'fa-3x'], Icon::FA) ?>
                <p class="text-justify"> Contiene informes relacionados a los productos que han traídos de los diversos eventos (ferias) con el fin de documentar las muestras de los proveedores. </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="jumbotron">
                <h3>Etiquetas</h3>
                <?= Icon::show('tags', ['class' => 'fa-3x'], Icon::FA) ?>
                <p class="text-justify"> Se muestra información de los eventos asociados a cada división general. </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="jumbotron">
                <h3> Eventos </h3>
                <?= Icon::show('calendar', ['class' => 'fa-3x'], Icon::FA) ?>
                <p class="text-justify"> Detalle de cada evento (feria) del cual se ha tenido información de productos y proveedores </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="jumbotron">
                <h3> Base de Datos </h3>
                <?= Icon::show('database', ['class' => 'fa-3x'], Icon::FA) ?>
                <p  class="text-justify"> Conglomerado de datos obtenidos de cada proveedor con el cual se tuvo con versación en cada evento (feria) </p>
            </div>
        </div>
    </div>
</div>
