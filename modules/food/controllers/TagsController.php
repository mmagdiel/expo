<?php

namespace app\modules\food\controllers;

use Yii;
use app\models\Tags;
use yii\web\Controller;
use app\models\search\TfoodSearch;
use yii\web\NotFoundHttpException;

/**
 * TagsController implements the CRUD actions for Tags model.
 */
class TagsController extends Controller
{
    /**
     * Lists all Tags models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TfoodSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tags model.
     * @param string $category_id
     * @param string $event_id
     * @return mixed
     */
    public function actionView($category_id, $event_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($category_id, $event_id),
        ]);
    }

    /**
     * Finds the Tags model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $category_id
     * @param string $event_id
     * @return Tags the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($category_id, $event_id)
    {
        if (($model = Tags::findOne(['category_id' => $category_id, 'event_id' => $event_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
