<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RecordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Records');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Record'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'company',
            'contact_information:ntext',
            'address:ntext',
            'products',
            // 'company_type',
            // 'category',
            // 'place',
            // 'latin_america',
            // 'note:ntext',
            // 'email:email',
            // 'date_first_contact',
            // 'date_answer',
            // 'city',
            // 'price_list',
            // 'product_catalog',
            // 'application_of_sanitary',
            // 'apostilled_legal',
            // 'clv',
            // 'manuals',
            // 'iso',
            // 'ce',
            // 'fda',
            // 'material_analysis',
            // 'tariff_classification',
            // 'created_at',
            // 'created_by',
            // 'updated_at',
            // 'updated_by',
            // 'event_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
