<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecordSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="record-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'company') ?>

    <?= $form->field($model, 'contact_information') ?>

    <?= $form->field($model, 'address') ?>

    <?= $form->field($model, 'products') ?>

    <?php // echo $form->field($model, 'company_type') ?>

    <?php // echo $form->field($model, 'category') ?>

    <?php // echo $form->field($model, 'place') ?>

    <?php // echo $form->field($model, 'latin_america') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'date_first_contact') ?>

    <?php // echo $form->field($model, 'date_answer') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'price_list') ?>

    <?php // echo $form->field($model, 'product_catalog') ?>

    <?php // echo $form->field($model, 'application_of_sanitary') ?>

    <?php // echo $form->field($model, 'apostilled_legal') ?>

    <?php // echo $form->field($model, 'clv') ?>

    <?php // echo $form->field($model, 'manuals') ?>

    <?php // echo $form->field($model, 'iso') ?>

    <?php // echo $form->field($model, 'ce') ?>

    <?php // echo $form->field($model, 'fda') ?>

    <?php // echo $form->field($model, 'material_analysis') ?>

    <?php // echo $form->field($model, 'tariff_classification') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'event_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
