<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Record */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="record-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_information')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'products')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'company_type')->dropDownList([ 'Pequeña' => 'Pequeña', 'Mediana' => 'Mediana', 'Grande' => 'Grande', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'place')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'latin_america')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_first_contact')->textInput() ?>

    <?= $form->field($model, 'date_answer')->textInput() ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_list')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'product_catalog')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'application_of_sanitary')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'apostilled_legal')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'clv')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'manuals')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'iso')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'ce')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'fda')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'material_analysis')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'tariff_classification')->dropDownList([ 'Si' => 'Si', 'No' => 'No', ], ['prompt' => 'Seleccionar una opción']) ?>

    <?= $form->field($model, 'event_id')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
