<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Event;
use app\models\Category;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($file, 'file')->fileInput() ?>

    <?= $form->field($tags, 'category_id')->dropDownList(
        ArrayHelper::map(Category::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una region'])  
    ?>

    <?= $form->field($tags, 'event_id')->dropDownList(
        ArrayHelper::map(Event::find()->all(),'id', 'name'),
        ['prompt'=>'Seleccione una region'])  
    ?>

    <div class="form-group">
        <?= Html::submitButton($file->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $file->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
