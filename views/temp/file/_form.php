<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\File */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="file-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($file, 'file')->fileInput() ?>

    <?= $form->field($event, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($event, 'logo')->widget('demi\image\FormImageWidget', [
        'imageSrc' => $event->getImageSrc('medium_'),
    ]) ?>

    <?= $form->field($event, 'date_from')->widget(DateRangePicker::className(), [
        'attributeTo' => 'date_to', 
        'form' => $form, // best for correct client validation
        'language' => 'es',
        'size' => 'lg',
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ] 
    ]);?>

    <?= $form->field($event, 'place')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($file->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $file->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
