<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/img/log.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            !Yii::$app->user->isGuest ? (
                ['label' => Yii::t('app', 'Admin'), 'items' => [
                        ['label' => Yii::t('app', 'Help'),'url' => ['/admin/']],
                        ['label' => Yii::t('app', 'Upload') . ' ' . Yii::t('app', 'Category'),'url' => ['/admin/category/create']],
                        ['label' => Yii::t('app', 'Upload') . ' ' . Yii::t('app', 'Events'),'url' => ['/admin/file/create']],
                        ['label' => Yii::t('app', 'Upload') . ' ' .  Yii::t('app', 'Samples'),'url' => ['/admin/file/upload']],
                        ['label' => Yii::t('app', 'Query') . ' ' . Yii::t('app', 'Etiquetas'),'url' => ['/admin/tags/index']],
                        ['label' => Yii::t('app', 'Query') . ' ' . Yii::t('app', 'Events'),'url' => ['/admin/event/index']],
                        ['label' => Yii::t('app', 'Query') . ' ' . Yii::t('app', 'Data Bases'),'url' => ['/admin/record/index']],
                        ['label' => Yii::t('app', 'Download') . ' ' . Yii::t('app', 'Files'),'url' => ['/admin/file/index']],
                        ['label' => Yii::t('app', 'Download') . ' ' . Yii::t('app', 'Suppliers'),'url' => ['/admin/file/data']],
                        ['label' => Yii::t('app', 'Download') . ' ' . Yii::t('app', 'Samples'),'url' => ['/admin/file/report']],
                        ['label' => Yii::t('app', 'Access Control') . ' ' . Yii::t('app', 'Role'),'url' => ['/mimin/role']],
                        ['label' => Yii::t('app', 'Access Control') . ' ' . Yii::t('app', 'Route'),'url' => ['/mimin/route']],
                        ['label' => Yii::t('app', 'Access Control') . ' ' . Yii::t('app', 'User'),'url' => ['/mimin/user']],
                    ]
                ]
            ) : (            
                ['label' => Yii::t('app', 'Signup'), 'url' => ['/site/signup']]
            )
            , Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    Yii::t('app', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end(); ?>

    <div> <?= Html::img('@web/img/fff.jpg', ['alt'=>Yii::$app->name]) ?></div>    
    <div class="container" >
        <div class="row"> 
            <div class="col-md-2">
                <?= Nav::widget([
                    'options' => ['class' => 'nav nav-pills nav-stacked'],
                    'items' => [
                        !Yii::$app->user->isGuest ? (
                            ['label' => Yii::t('app', 'Food'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/food/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/food/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/food/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/food/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/food/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        ) 
                        ,!Yii::$app->user->isGuest ? (
                            ['label' => Yii::t('app', 'Odontology'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/odontology/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/odontology/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/odontology/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/odontology/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/odontology/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )
                        ,!Yii::$app->user->isGuest ? (
                            ['label' =>  Yii::t('app', 'Medical'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/medical/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/medical/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/medical/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/medical/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/medical/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )
                        ,!Yii::$app->user->isGuest ? (
                            ['label' =>  Yii::t('app', 'Care'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/care/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/care/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/care/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/care/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/care/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )
                        ,!Yii::$app->user->isGuest ? (
                            ['label' =>  Yii::t('app', 'Cosmetics'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/cosmetics/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/cosmetics/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/cosmetics/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/cosmetics/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/cosmetics/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )
                        ,!Yii::$app->user->isGuest ? (
                            ['label' =>  Yii::t('app', 'Pharmacist'), 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/pharmacist/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/pharmacist/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/pharmacist/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/pharmacist/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/pharmacist/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )
                        ,!Yii::$app->user->isGuest ? (
                            ['label' => 'Fitness', 'items' => [
                                    ['label' => Yii::t('app', 'Help'),'url' => ['/fitness/']],
                                    ['label' => Yii::t('app', 'Samples'),'url' => ['/fitness/file/index']],
                                    ['label' => Yii::t('app', 'Tags'),'url' => ['/fitness/tags/index']],
                                    ['label' => Yii::t('app', 'Events'),'url' => ['/fitness/event/index']],
                                    ['label' => Yii::t('app', 'Data Bases'),'url' => ['/fitness/record/index']],
                                ]
                            ]
                        ) : (            
                            ''
                        )] 
                    ]);
                ?>
            </div>      
            <div class="col-md-10">  
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Premiere Soluciones Integrales C.A. <?= date('Y') ?></p>
        
        <p class="pull-right">&emsp; Colaboradores: Marifé Reyes y Gerardo Machado.</p>
        <p class="pull-right">Desarrollado por el Lic. Magdiel Márquez</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
