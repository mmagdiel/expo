<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <?=
            Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name]);
        ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Files') ?></h2>

                    <p> Cargado al sistema.</p>
                
                <p><a class="btn btn-default" href="<?= Url::to(['file/index']);?>"><?= Yii::t('app', 'Files') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Suppliers') ?></h2>

                    <p> Descarga la data los eventos</p>

                <p><a class="btn btn-default" href="<?= Url::to(['event/report']);?>"><?= Yii::t('app', 'Suppliers') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Samples') ?></h2>

                    <p> Asociado a los eventos..</p>

                <p><a class="btn btn-default" href="<?= Url::to(['event/report']);?>"><?= Yii::t('app', 'Samples') ?> &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
