<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <?=
            Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name]);
        ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Tags') ?></h2>

                    <p> De los eventos dependiendo de su categorias </p>
                
                <p><a class="btn btn-default" href="<?= Url::to(['tags/index']);?>"><?= Yii::t('app', 'Tags') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Events') ?></h2>

                    <p> La información del nombre, logo, fecha de los eventos.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['event/index']);?>"><?= Yii::t('app', 'Events') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Data Bases') ?></h2>

                    <p> Información de contacto registrado en los eventos.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['record/index']);?>"><?= Yii::t('app', 'Data Bases') ?> &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
