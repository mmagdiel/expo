<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <?=
            Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name]);
        ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Category') ?></h2>

                <p> Crea y verifica las categorias a la cual pertenece cada evento.</p>
                
                <p><a class="btn btn-default" href="<?= Url::to(['category/create']);?>"><?= Yii::t('app', 'Category') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Events') ?></h2>

                    <p> Carga el archivo de excel con la información del evento y sus registros.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['file/create']);?>"><?= Yii::t('app', 'Events') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Samples') ?></h2>

                    <p> Asociar al evento las categorias correspondiente junto sus informes.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['file/upload']);?>"><?= Yii::t('app', 'Samples') ?> &raquo;</a></p>
            </div>
        </div>

    </div>
</div>