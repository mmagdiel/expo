<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <div class="jumbotron">
        <?=
            Html::img('@web/img/logo.jpg', ['alt'=>Yii::$app->name]);
        ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-3">
                <h2><?= Yii::t('app', 'Upload') ?></h2>

                <p> La información, siguiendo estos paso...</p>
                
                <p><a class="btn btn-default" href="<?= Url::to(['site/update']);?>"><?= Yii::t('app', 'Upload') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-5">
                <h2><?= Yii::t('app', 'Query') ?></h2>

                    <p> La información de los eventos, categorias y registros de interes.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['site/main']);?>"><?= Yii::t('app', 'Query') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('app', 'Download') ?></h2>

                    <p> Reportes y resumenes sobre los eventos.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['site/review']);?>"><?= Yii::t('app', 'Download') ?> &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
