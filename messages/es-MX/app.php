<?php
	return [
        'Ce' => 'Ce',
        'Fda' => 'Fda',
        'Clv' => 'Clv',
        'Iso' => 'Iso',
		'Note' => 'Notas',
		'Place' => 'Lugar',
        'City' => 'Ciudad',
		'Size' => 'Tamaño',
		'Home' => 'Inicio',
		'Send' => 'Enviar',
		'Name' => 'Nombre',
		'Subject' => 'Tema',
		'Start' => 'Inicio',
		'Event' => 'Evento',
		'File' => 'Archivo',
		'Upload' => 'Carga',
		'Help' => 'Detalles',
		'Submit' => 'Enviar',
		'Global' => 'Buscar',
		'Food' => 'Alimento',
		'Search' => 'Buscar',
		'Files' => 'Archivos',
		'Medical' => 'Médico',
		'Events' => 'Eventos',
		'Query' => 'Consulta',
		'Tags' => 'Etiquetas',
		'Reset' => 'Reinicio',
		'Events' => 'Eventos',
		'Fitness' => 'Deporte',
		'Signup' => 'Registro',
		'People' => 'Personas',
		'Record' => 'Registro',
		'Company' => 'Compañia',
		'Samples' => 'Muestras',
		'Contact' => 'Contacto',
        'Manuals' => 'Manuales',
		'Address' => 'Dirección',
		'Download' => 'Descarga',
		'Descarga' => 'Download',
		'Records' => 'Registros',
		'Products' => 'Productos',
		'Cosmetics' => 'Cosmético',
		'Pharmacist' => 'Farmacia',
		'Category' => 'Categorias',
		'Admin' => 'Administrador',
		'Date To' => 'Fecha hasta',
		'Created At' => 'Creado en',
		'Logout' => 'Cerrar Sesión',
		'Login' => 'Iniciar Sesión',
		'Care' => 'Cuidado Personal',
		'Created By' => 'Creado por',
		'Suppliers' => 'Proveedores',
		'Date From' => 'Fecha desde',
		'Defenition' => 'Definición',
		'Odontology' => 'Odontología', 
		'Event ID' => 'Nombre Evento',
		'Remember Me' => ' Recordarme',
		'Data Bases' => 'Base de Datos',
		'Email' => 'Correo Electronico',
		'Updated At' => 'Actualizado en',
		'Updated By' => 'Actualizado por',
        'Price List' => 'Lista de Precios',
		'Company Type' => 'Tipo de Compañia',
        'Date Answer' => 'Fecha de Respuesta',
		'Latin America' => '¿America Latina?',
		'Access Control' => 'Control de Acceso',
		'Category ID' => 'Nombre de la Categoria',
		'Number of Records'=> 'Numero de Registros',
        'Product Catalog' => 'Catalogo de Productos',
        'Material Analysis' => 'Analisis de Material',
		'Date First Contact' => 'Fecha de 1er Contacto',
		'Number of Documents' => 'Numero de Documentos',
        'Apostilled Legal' => 'Poder Legal Apostillado',
		'Verification Code' => 'Codigo de Verificación',
		'Contact Information' => 'Información de contacto',
		'Request password reset' => 'Reiniciar la contraseña',
        'Tariff Classification' => 'Clasificación Arancelaria',
        'Application Of Sanitary' => 'Solicitud de Doc Reg Sanitario',
		'Please fill out the following fields to' => 'Por favor, rellene los campos para acceder', 
		'Please fill out your email. A link to reset password will be sent there.' => 'Por favor, rellene con su correo electronico. Un enlace para reestablecer su contraseña será enviado.',
		'If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.' => 'Si usted tiene 
alguna pregunta o duda, por favor rellene el siguiente formulario para contactarnos con usted. Gracias ',
	];
?>