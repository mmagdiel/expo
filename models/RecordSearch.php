<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Record;

/**
 * RecordSearch represents the model behind the search form about `app\models\Record`.
 */
class RecordSearch extends Record
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by', 'event_id'], 'integer'],
            [['company', 'contact_information', 'address', 'products', 'company_type', 'category', 'place', 'latin_america', 'note', 'email', 'date_first_contact', 'date_answer', 'city', 'price_list', 'product_catalog', 'application_of_sanitary', 'apostilled_legal', 'clv', 'manuals', 'iso', 'ce', 'fda', 'material_analysis', 'tariff_classification'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Record::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_first_contact' => $this->date_first_contact,
            'date_answer' => $this->date_answer,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'event_id' => $this->event_id,
        ]);

        $query->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'contact_information', $this->contact_information])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'products', $this->products])
            ->andFilterWhere(['like', 'company_type', $this->company_type])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'latin_america', $this->latin_america])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'price_list', $this->price_list])
            ->andFilterWhere(['like', 'product_catalog', $this->product_catalog])
            ->andFilterWhere(['like', 'application_of_sanitary', $this->application_of_sanitary])
            ->andFilterWhere(['like', 'apostilled_legal', $this->apostilled_legal])
            ->andFilterWhere(['like', 'clv', $this->clv])
            ->andFilterWhere(['like', 'manuals', $this->manuals])
            ->andFilterWhere(['like', 'iso', $this->iso])
            ->andFilterWhere(['like', 'ce', $this->ce])
            ->andFilterWhere(['like', 'fda', $this->fda])
            ->andFilterWhere(['like', 'material_analysis', $this->material_analysis])
            ->andFilterWhere(['like', 'tariff_classification', $this->tariff_classification]);

        return $dataProvider;
    }
}
