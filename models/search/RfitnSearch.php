<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Record;
use app\models\Tags;

/**
 * RecordSearch represents the model behind the search form about `app\models\Record`.
 */
class RfitnSearch extends Record
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['company', 'date_first_contact', 'contact_information', 'event_id', 'address', 'products', 'company_type', 'category', 'place', 'latin_america', 'note'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = Tags::find()
            ->where(['category_id' => 5])
            ->asArray()
            ->all();

        $event = []; 
        foreach ($q as $key => $value) {
            $event[] = $q[$key]['event_id'];
        } 

        $query = Record::find()
            ->andWhere(['in', 'event_id', $event]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('event');

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_first_contact' => $this->date_first_contact,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'company', $this->company])
            ->andFilterWhere(['like', 'contact_information', $this->contact_information])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'products', $this->products])
            ->andFilterWhere(['like', 'company_type', $this->company_type])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'latin_america', $this->latin_america])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'event.name', $this->event_id]);

        return $dataProvider;
    }
}
