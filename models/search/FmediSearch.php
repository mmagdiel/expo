<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use app\models\File;
use app\models\Tags;
use yii\data\ActiveDataProvider;

/**
 * FileSearch represents the model behind the search form about `app\models\File`.
 */
class FmediSearch extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'size', 'error', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['path', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $q = Tags::find()
            ->where(['category_id' => 1])
            ->asArray()
            ->all();

        $event = []; 
        foreach ($q as $key => $value) {
            $event[] = $q[$key]['event_id'];
        } 

        $query = File::find()
                ->where(['type' => 'Documentos'])
                ->andWhere(['in', 'event_id', $event]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'size' => $this->size,
            'error' => $this->error,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'path', $this->path])
            ->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
