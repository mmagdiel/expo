<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "record".
 *
 * @property string $id
 * @property string $company
 * @property string $contact_information
 * @property string $address
 * @property string $products
 * @property string $company_type
 * @property string $category
 * @property string $place
 * @property string $latin_america
 * @property string $note
 * @property string $email 
 * @property string $date_first_contact 
 * @property string $date_answer 
 * @property string $city 
 * @property string $price_list 
 * @property string $product_catalog 
 * @property string $application_of_sanitary 
 * @property string $apostilled_legal 
 * @property string $clv 
 * @property string $manuals 
 * @property string $iso 
 * @property string $ce 
 * @property string $fda 
 * @property string $material_analysis 
 * @property string $tariff_classification 
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 * @property string $event_id
 *
 * @property Event $event
 */
class Record extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     *  Public doc   
     */    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            BlameableBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_information', 'address', 'company_type', 'latin_america', 'note', 'price_list', 'product_catalog', 'application_of_sanitary', 'apostilled_legal', 'clv', 'manuals', 'iso', 'ce', 'fda', 'material_analysis', 'tariff_classification'], 'string'],
            [['date_first_contact', 'date_answer'], 'safe'],
            [['created_at', 'created_by', 'updated_at', 'updated_by', 'event_id'], 'integer'],
            [['event_id'], 'required'],
            [['company', 'products', 'category', 'email'], 'string', 'max' => 257],
            [['place', 'city'], 'string', 'max' => 127],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company' => Yii::t('app', 'Company'),
            'contact_information' => Yii::t('app', 'Contact Information'),
            'address' => Yii::t('app', 'Address'),
            'products' => Yii::t('app', 'Products'),
            'company_type' => Yii::t('app', 'Company Type'),
            'category' => Yii::t('app', 'Category'),
            'place' => Yii::t('app', 'Place'),
            'latin_america' => Yii::t('app', 'Latin America'),
            'note' => Yii::t('app', 'Note'),
            'email' => Yii::t('app', 'Email'),
            'date_first_contact' => Yii::t('app', 'Date First Contact'),
            'date_answer' => Yii::t('app', 'Date Answer'),
            'city' => Yii::t('app', 'City'),
            'price_list' => Yii::t('app', 'Price List'),
            'product_catalog' => Yii::t('app', 'Product Catalog'),
            'application_of_sanitary' => Yii::t('app', 'Application Of Sanitary'),
            'apostilled_legal' => Yii::t('app', 'Apostilled Legal'),
            'clv' => Yii::t('app', 'Clv'),
            'manuals' => Yii::t('app', 'Manuals'),
            'iso' => Yii::t('app', 'Iso'),
            'ce' => Yii::t('app', 'Ce'),
            'fda' => Yii::t('app', 'Fda'),
            'material_analysis' => Yii::t('app', 'Material Analysis'),
            'tariff_classification' => Yii::t('app', 'Tariff Classification'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'event_id' => Yii::t('app', 'Event ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\query\RecordQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\RecordQuery(get_called_class());
    }
}
