<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\db\Query;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\console\Exception;
use yii\console\Controller;
use yii\db\Migration;
use DateTime;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class UploadController extends Controller
{
  /**
   * This command echoes what you have entered as the message.
   * @param string $message the message to be echoed.
   */
  public function actionRecord($id, $idE, $user)
  {
      // if (Yii::$app instanceof Yii\console\Application) -> no me funciono
      // Guard Clause, validating that $ id exists
  	if ( !isset($id) ){
  		throw new Exception("Id empty()");
  	}
  	$file = (new Query())
  		->select(['path'])
  		->from('file')
  		->where(['id' => $id])
  		->all();
    $inputFile = 'C:\xampp\htdocs\expo\web/' . $file[0]['path'];
      // Read and save in $dota the excel file
      try{
        $dota = \moonland\phpexcel\Excel::import($inputFile,[
          'setFirstRecordAsKeys' => false,
          'setIndexSheetByName' => true, 
          'getOnlySheet' => 'sheet1'
        ]);
      }
      catch(Exception $err){
        throw new Exception("Id empty()");
      }
      $row = [];
      // Extracting the columns of $dota in arrays
      foreach ($dota as $key => $value ) {
        if ($key == 1){
            continue;
        }
        if ( ArrayHelper::getValue($dota[$key], 'A') === null &&
             ArrayHelper::getValue($dota[$key], 'B') === null &&
             ArrayHelper::getValue($dota[$key], 'C') === null &&
             ArrayHelper::getValue($dota[$key], 'D') === null &&
             ArrayHelper::getValue($dota[$key], 'E') === null &&
             ArrayHelper::getValue($dota[$key], 'F') === null &&
             ArrayHelper::getValue($dota[$key], 'G') === null &&
             ArrayHelper::getValue($dota[$key], 'H') === null &&
             ArrayHelper::getValue($dota[$key], 'I') === null &&
             ArrayHelper::getValue($dota[$key], 'J') === null &&
             ArrayHelper::getValue($dota[$key], 'K') === null &&
             ArrayHelper::getValue($dota[$key], 'L') === null &&
             ArrayHelper::getValue($dota[$key], 'M') === null &&
             ArrayHelper::getValue($dota[$key], 'N') === null &&
             ArrayHelper::getValue($dota[$key], 'O') === null &&
             ArrayHelper::getValue($dota[$key], 'P') === null &&
             ArrayHelper::getValue($dota[$key], 'Q') === null &&
             ArrayHelper::getValue($dota[$key], 'R') === null &&
             ArrayHelper::getValue($dota[$key], 'S') === null &&
             ArrayHelper::getValue($dota[$key], 'T') === null &&
             ArrayHelper::getValue($dota[$key], 'U') === null &&
             ArrayHelper::getValue($dota[$key], 'V') === null &&
             ArrayHelper::getValue($dota[$key], 'W') === null &&
             ArrayHelper::getValue($dota[$key], 'X') === null 
          ) {
              continue;
          }
        $row[] = [
          ArrayHelper::getValue($dota[$key], 'A'),
          ArrayHelper::getValue($dota[$key], 'B'),
          ArrayHelper::getValue($dota[$key], 'C'),
          ArrayHelper::getValue($dota[$key], 'D'),
          ArrayHelper::getValue($dota[$key], 'E'),
          ArrayHelper::getValue($dota[$key], 'F'),
          ArrayHelper::getValue($dota[$key], 'G'),
          ucfirst( ArrayHelper::getValue($dota[$key], 'H')),
          ArrayHelper::getValue($dota[$key], 'I'),
          ArrayHelper::getValue($dota[$key], 'J'),
          ArrayHelper::getValue($dota[$key], 'K'),
          ArrayHelper::getValue($dota[$key], 'L'),
          ArrayHelper::getValue($dota[$key], 'M'),
          ucfirst( ArrayHelper::getValue($dota[$key], 'N')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'O')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'P')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'Q')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'R')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'S')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'T')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'U')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'V')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'W')),
          ucfirst( ArrayHelper::getValue($dota[$key], 'X')),
          time(),
          $user,
          time(),
          $user,
          $idE
        ];
      }
      // Grouping the arrays into an matrix
      $columms = [
        'company',
        'contact_information',
        'address',
        'products',
        'company_type',
        'category',
        'place',
        'latin_america',
        'note',
        'email',
        'date_first_contact',
        'date_answer',
        'city',
        'price_list',
        'product_catalog',
        'application_of_sanitary',
        'apostilled_legal',
        'clv',
        'manuals',
        'iso',
        'ce', 
        'fda',
        'material_analysis', 
        'tariff_classification',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
        'event_id',
      ];

      Yii::$app
          ->db
          ->createCommand()
          ->batchInsert('record', $columms, $row)
          ->execute();
  }
}
